package
{
	import com.kazike.fgj2017.Main;
	import com.kazike.fgj2017.screens.MainScreen;
	
	import flash.display.Sprite;
	
	[SWF(frameRate="60", width="1024", height="600", backgroundColor="0xFFFFFF")]
	public class FGJ2017Web extends Sprite
	{
		public function FGJ2017Web()
		{
			new Main(stage);
		}
	}
}