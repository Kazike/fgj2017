package com.kazike.fgj2017.utils
{
	public function checkProbablity(factor:Number):Boolean
	{
		if(factor < 0 || factor > 1) {
			throw new Error("FACTOR MUST BE BETWEEN 0 AND 1");
		}
		
		return Math.random() < factor;
	}
}