package com.kazike.fgj2017.utils
{
	public function getRandomItem(arr:Array, remove:Boolean = true):*
	{
		var index:int = Math.floor(Math.random() * arr.length);
		
		var res:* = arr[index];
		if(remove) {
			arr.splice(index, 1);
		}
		
		return res;
	}
}