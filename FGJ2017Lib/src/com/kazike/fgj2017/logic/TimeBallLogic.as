package com.kazike.fgj2017.logic
{
	import com.kazike.fgj2017.items.TimeBall;
	
	import flash.utils.Dictionary;

	public class TimeBallLogic
	{
		// -------------------------------------------------------------------------
		// TIME BALL TYPES
		private static const GOOD_TYPES:Array = ["good_1", "good_2", "good_3"];
		public static const GOOD_1_TYPE:String = "good_1";
		public static const GOOD_2_TYPE:String = "good_2";
		public static const GOOD_3_TYPE:String = "good_3";
		
		private static const BAD_TYPES:Array = ["bad_1", "bad_2", "bad_3"];
		public static const BAD_1_TYPE:String = "bad_1";
		public static const BAD_2_TYPE:String = "bad_2";
		public static const BAD_3_TYPE:String = "bad_3";
		
		private static const SPECIAL_TYPES:Array = ["special_ser", "special_tener", "special_hacer", "special_relaciones"];
		public static const SPECIAL_SER_TYPE:String = "special_ser";
		public static const SPECIAL_TENER_TYPE:String = "special_tener";
		public static const SPECIAL_HACER_TYPE:String = "special_hacer";
		public static const SPECIAL_RELACIONES_TYPE:String = "special_relaciones";
		
		// -------------------------------------------------------------------------
		// TIME BALL VALUES
		public static const GOOD_1_VALUE:int = 20;
		public static const GOOD_2_VALUE:int = 50;
		public static const GOOD_3_VALUE:int = 100;
		
		public static const BAD_1_VALUE:int = -20;
		public static const BAD_2_VALUE:int = -50;
		public static const BAD_3_VALUE:int = -100;
		
		
		// -------------------------------------------------------------------------
		// TIME BALL OBJECTS
		private static var _timeBallObjects:Dictionary;
		
		public function TimeBallLogic()
		{
		}
		
		public static function initialize():void {
			_timeBallObjects = new Dictionary();
			
			_timeBallObjects[GOOD_1_TYPE] = {text: "Something good", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: GOOD_1_VALUE};
			_timeBallObjects[GOOD_2_TYPE] = {text: "Something great", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: GOOD_2_VALUE};
			_timeBallObjects[GOOD_3_TYPE] = {text: "Something fantastic", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: GOOD_3_VALUE};
			
			_timeBallObjects[BAD_1_TYPE] = {text: "Something bad", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: BAD_1_VALUE};
			_timeBallObjects[BAD_2_TYPE] = {text: "Something worse", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: BAD_2_VALUE};
			_timeBallObjects[BAD_3_TYPE] = {text: "Something terrible", 
				targetGroups: [LiveBallLogic.GROUP_PRIMARY, LiveBallLogic.GROUP_SECONDARY, LiveBallLogic.GROUP_FINAL],
				value: BAD_3_VALUE};
			
			_timeBallObjects[SPECIAL_HACER_TYPE] = {text: "Something to be proud of", targetID: "hacer"};
			_timeBallObjects[SPECIAL_TENER_TYPE] = {text: "Something you don't need anymore", targetID: "tener"};
			_timeBallObjects[SPECIAL_RELACIONES_TYPE] = {text: "Somebody to remember forever", targetID: "relaciones"};
			_timeBallObjects[SPECIAL_SER_TYPE] = {text: "Something to accept", targetID: "ser"};
		}
		
		public static function createTimeBall(type:String):TimeBall
		{
			var obj:Object = _timeBallObjects[type];
			var res:TimeBall;
			
			if(SPECIAL_TYPES.indexOf(type) >= 0) {
				res = new TimeBall(type, [obj.targetID], obj.text, 0);
			}
			else {
				res = new TimeBall(type, obj.targetGroups, obj.text, obj.value);
			}
			
			return res;
		}
		
		public static function isGoodType(timeBall:TimeBall):Boolean
		{
			return GOOD_TYPES.indexOf(timeBall.type) >= 0;
		}
		
		public static function isBadType(timeBall:TimeBall):Boolean
		{
			return BAD_TYPES.indexOf(timeBall.type) >= 0;
		}
		
		public static function isSpecialType(timeBall:TimeBall):Boolean
		{
			return SPECIAL_TYPES.indexOf(timeBall.type) >= 0;
		}
	}
}