package com.kazike.fgj2017.logic
{
	import com.kazike.fgj2017.items.LiveBall;
	
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;

	public class LiveBallLogic
	{
		[Embed(source="../data/life.json", mimeType="application/octet-stream")]
		private static const LIFE_DATA:Class;
		
		// -------------------------------------------------------------------------
		// LIFE BALL IDS
		public static const ID_YOU:String = "you";
		
		public static const ID_RELACIONES:String = "relaciones";
		
		public static const ID_RELACIONES_FAMILIA:String = "familia";
		public static const ID_RELACIONES_PAREJA:String = "pareja";
		public static const ID_RELACIONES_AMIGOS:String = "amigos";
		
		public static const ID_HACER:String = "hacer";
		
		public static const ID_HACER_EXPERIENCIAS:String = "experiencias";
		public static const ID_HACER_SUENO:String = "sueno";
		public static const ID_HACER_TRABAJO:String = "trabajo";
		
		public static const ID_SER:String = "ser";
		
		public static const ID_SER_CUERPO:String = "cuerpo";
		public static const ID_SER_MENTE:String = "mente";
		public static const ID_SER_ALMA:String = "alma";
		
		public static const ID_TENER:String = "tener";
		
		public static const ID_TENER_DINERO:String = "dinero";
		public static const ID_TENER_CASA:String = "casa";
		public static const ID_TENER_COSAS:String = "cosas";
		
		
		// -------------------------------------------------------------------------
		// LIFE BALL TYPES
		public static const TYPE_RELACIONES:String = "relaciones";
		public static const TYPE_HACER:String = "hacer";
		public static const TYPE_SER:String = "ser";
		public static const TYPE_TENER:String = "tener";
		
		// -------------------------------------------------------------------------
		// LIFE BALL GROUPS
		public static const GROUP_YOU:String = "you";
		public static const GROUP_PRIMARY:String = "primary";
		public static const GROUP_SECONDARY:String = "secondary";
		public static const GROUP_FINAL:String = "final";
		
		// -------------------------------------------------------------------------
		// LIFE BALL STATUS
		public static const STATUS_INACTIVE:String = "inactive";
		public static const STATUS_ACTIVE:String = "active";
		public static const STATUS_DEAD:String = "dead";
		
		// -------------------------------------------------------------------------
		// LIFE BALL ENERGY
		private static const ENERGY_FOR_PRIMARY_GROUP:Number = 100;
		private static const ENERGY_FOR_SECONDARY_GROUP:Number = 50;
		private static const ENERGY_FOR_FINAL_GROUP:Number = 25;
		
		// -------------------------------------------------------------------------
		// LIFE BALL OBJECTS
		private static var _liveBallObjects:Dictionary; // By ID
		
		private static var _availableLiveBalls:Dictionary; // Array of available for others (by ID)
		
		
		public function LiveBallLogic()
		{
		}
		
		public static function initialize():void {
			_liveBallObjects = new Dictionary();
			_availableLiveBalls = new Dictionary();
			
			var lifeData:ByteArray = new LIFE_DATA() as ByteArray;
			var lifeDataObj:Object = JSON.parse(lifeData.toString());
			
			var i:int;
			var lifeObj:Object;
			for(i = 0; i < lifeDataObj.lifeBalls.length; i++) {
				lifeObj = lifeDataObj.lifeBalls[i];
				
				_liveBallObjects[lifeObj.id] = lifeObj;
				
				if(!_availableLiveBalls[lifeObj.parentID]) _availableLiveBalls[lifeObj.parentID] = [];
				_availableLiveBalls[lifeObj.parentID].push(lifeObj.id);
			}
		}
		
		public static function getParentIDForChildLiveBall(id:String):String
		{
			var res:String;
			
			for each(var lifeObj:Object in _liveBallObjects) {
				if(lifeObj.id == id) {
					res = lifeObj.parentID;
					break;
				}
			}
			
			return res;
		}
		
		public static function getRandomChildLiveBallID(parentID:String):String
		{
			var availableIDs:Array = _availableLiveBalls[parentID];
			if(availableIDs.length <= 0) return null;
			
			var randomID:String = availableIDs[Math.floor(Math.random() * availableIDs.length)];
			return randomID;
		}
		
		public static function createRandomLiveBall(parentID:String, status:String, energy:Number):LiveBall
		{
			if(!hasAvailableLiveBall(parentID)) {
				throw new Error("NO MORE BALLS!");
			}
			
			var availableIDs:Array = _availableLiveBalls[parentID];
			var randomID:String = availableIDs[Math.floor(Math.random() * availableIDs.length)];
			
			return createSpecificLiveBall(randomID, status, energy);
		}
		
		public static function createSpecificLiveBall(id:String, status:String, energy:Number):LiveBall
		{
			var obj:Object = _liveBallObjects[id];
			
			var targetEnergy:Number = energy > 0 ? energy : obj.energy;
			
			var res:LiveBall = new LiveBall(obj.id, obj.parentID, obj.type, obj.group, obj.text, targetEnergy, status);
			
			removeAvailableLiveBall(obj.parentID, obj.id);
			
			return res;
		}
		
		public static function hasAvailableLiveBall(parentID:String):Boolean
		{
			return (_availableLiveBalls[parentID] ? _availableLiveBalls[parentID].length > 0 : false);
		}
		
		public static function recoverAvailableLiveBall(parentID:String, id:String):void
		{
			if(!_availableLiveBalls[parentID] || _availableLiveBalls[parentID].indexOf(id) >= 0) {
				throw new Error("NO PARENT DEFINED OR ALREADY CONTAINING!");
			}
			
			_availableLiveBalls[parentID].push(id);
		}
		
		private static function removeAvailableLiveBall(parentID:String, id:String):void
		{
			if(!_availableLiveBalls[parentID] || _availableLiveBalls[parentID].indexOf(id) < 0) {
				return;
			}
			
			_availableLiveBalls[parentID].splice(_availableLiveBalls[parentID].indexOf(id), 1);
		}
	}
}