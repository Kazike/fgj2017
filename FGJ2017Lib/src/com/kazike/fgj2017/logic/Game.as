package com.kazike.fgj2017.logic
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.easing.BackIn;
	import com.greensock.easing.BackOut;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Sine;
	import com.greensock.easing.SineInOut;
	import com.greensock.easing.SineOut;
	import com.kazike.fgj2017.YouBallMC;
	import com.kazike.fgj2017.items.LiveBall;
	import com.kazike.fgj2017.items.TextBall;
	import com.kazike.fgj2017.items.TimeBall;
	import com.kazike.fgj2017.utils.checkProbablity;
	import com.kazike.fgj2017.utils.getRandomItem;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	
	public class Game extends Sprite
	{
		// LOGIC
		private static const INITIAL_LIFE_BALL_LIFE:Number = 50;
		private static const INITIAL_LIFE_BALL_FINAL_LIFE:Number = 50;
		
		public static const MAX_SECONDARY_LIFE_BALLS_PER_PARENT:int = 3;
		public static const MAX_FINAL_LIFE_BALLS_PER_PARENT:int = 4;
		public static const MAX_LIFE_BALL_LIFE:Number = 100;
		public static const MUTATE_LIFE_BALL_LIFE:Number = 80;
		
		private static const REMOVED_ENERGY_UNATTENDED_CHILDHOOD:Number = -2;
		private static const REMOVED_ENERGY_UNATTENDED_TEENAGER:Number = -5;
		private static const REMOVED_ENERGY_UNATTENDED_ADULT:Number = -7;
		private static const REMOVED_ENERGY_UNATTENDED_OLD_AGE:Number = -10;
		
		// LIVE BALLS POSITIONS
		private const LIFE_BALLS_REL_POSITION:Object = {x: 0.5, y: 0.53};
		
		private const LB_PRIMARY_OFFSET:Object = {x: 60, y: 60};
		private const LB_SECONDARY_OFFSETS:Array = [
			{x: -10, y: 120},
			{x: 90, y: 90},
			{x: 120, y: -10}
			];
		private const LB_FINAL_INITIAL_OFFSET:Number = 150;
		private const LB_FINAL_SECUENTIAL_OFFSET:Number = 60;
		
		// TIME BALLS POSITIONS
		private const TIME_BALLS_SEPARATION:Number = 70;
		private const TIME_BALLS_POSITION:Object = {x: 210, y: 55};
		
		// TEXT POSITIONS
		private const TEXT_REL_POSITION_X:Number = 0.5;
		private const TEXT_POSITION_BOTTOM:Number = 30;
		
		// Containers
		private var _youContainer:Sprite;
		private var _timeBallsContainerContainer:Sprite;
		private var _timeBallsContainer:Sprite;
		private var _lifeBallsContainer:Sprite;
		private var _textContainer:Sprite;
		
		// Logic
		private var _timeBalls:Array;
		private var _lifeBalls:Dictionary;
		private var _lifeBallsSecondaryAssoc:Dictionary;
		private var _lifeBallsFinalAssoc:Dictionary;
		private var _lifeBallsPrimaryPosAvailable:Array;
		private var _lifeBallsSecondaryPosAvailable:Dictionary;
		private var _lifeBallsFinalPosAvailable:Dictionary;
		
		private var _selectedTimeBall:TimeBall;
		private var _timeBallsTimeline:TimelineLite;
		private var _auxPoint:Point = new Point;
		
		private var _text:TextBall;
		
		// ----
		private var _isGameLost:Boolean;
		private var _isGameLostBallsCreated:Boolean;
		private var _isGameFinished:Boolean;
		
		private var _timeBallsAdded:int;
		private var _fixedInitialLifeBalls:Array;
		private var _isShowingNewLifeBall:Boolean;
		
		private var _removedEnergyByNotAttending:Number;
		private var _addedTimeBalls:int;
		private var _lastConsecutiveAddedGoodTimeBalls:int;
		private var _lastConsecutiveAddedBadTimeBalls:int;
		
		
		public function Game()
		{
			super();
			
			initialize();
		}
		
		public function setHidden():void
		{
			_youContainer.alpha = 0;
			_timeBallsContainerContainer.x = -400;
		}
		
		public function show():void
		{
			// FAKE TIMEBALL
			var timeBall:TimeBall = TimeBallLogic.createTimeBall(TimeBallLogic.GOOD_3_TYPE);
			timeBall.x = TIME_BALLS_SEPARATION;
			_timeBallsContainer.addChild(timeBall);
			
			var tbDestPos:Point = new Point(_lifeBallsContainer.x, _lifeBallsContainer.y);
			tbDestPos = tbDestPos.subtract(new Point(_timeBallsContainer.x, _timeBallsContainer.y));
			
			var tl:TimelineLite = new TimelineLite({delay: 1, onComplete: onComplete});
			tl.add(TweenLite.to(_youContainer, 1, {alpha: 1, ease: Linear.easeNone}));
			tl.add(TweenLite.to(_timeBallsContainerContainer, 3, {x: 0, ease: SineOut.ease}));
			tl.add(TweenLite.to(timeBall, 2, {x: tbDestPos.x, y: tbDestPos.y, ease: SineInOut.ease}));
			tl.add(TweenLite.to(timeBall, 1, {scaleX: 0, scaleY: 0, ease: BackIn.ease}));
			
			function onComplete():void
			{
				_timeBallsContainer.removeChild(timeBall);
				start();
			}
		}
		
		// -------------------------------------------------------------------------------
		// LOGIC
		
		private function initialize():void {
			_isGameLost = false;
			_isGameLostBallsCreated = false;
			_isGameFinished = false;
			_timeBallsAdded = 0;
			_isShowingNewLifeBall = false;
			
			_timeBalls = new Array;
			_lifeBalls = new Dictionary;
			_lifeBallsSecondaryAssoc = new Dictionary;
			_lifeBallsFinalAssoc = new Dictionary;
			_lifeBallsPrimaryPosAvailable = [
				{x: LB_PRIMARY_OFFSET.x, 	y: LB_PRIMARY_OFFSET.y, 	xFactor: 1, yFactor: 1},
				{x: -LB_PRIMARY_OFFSET.x, 	y: LB_PRIMARY_OFFSET.y,		xFactor: -1, yFactor: 1},
				{x: LB_PRIMARY_OFFSET.x,	y: -LB_PRIMARY_OFFSET.y,	xFactor: 1, yFactor: -1},
				{x: -LB_PRIMARY_OFFSET.x,	y: -LB_PRIMARY_OFFSET.y,	xFactor: -1, yFactor: -1}
				];
			_lifeBallsSecondaryPosAvailable = new Dictionary;
			_lifeBallsFinalPosAvailable = new Dictionary;
			
			// Containers
			_youContainer = new Sprite;
			_youContainer.addChild(new YouBallMC);
			_youContainer.x = Constants.WIDTH * LIFE_BALLS_REL_POSITION.x;
			_youContainer.y = Constants.HEIGHT * LIFE_BALLS_REL_POSITION.y;
			addChild(_youContainer);
			
			_lifeBallsContainer = new Sprite;
			_lifeBallsContainer.x = Constants.WIDTH * LIFE_BALLS_REL_POSITION.x;
			_lifeBallsContainer.y = Constants.HEIGHT * LIFE_BALLS_REL_POSITION.y;
			addChild(_lifeBallsContainer);
			
			_timeBallsContainerContainer = new Sprite;
			addChild(_timeBallsContainerContainer);
			_timeBallsContainer = new Sprite;
			_timeBallsContainer.x = TIME_BALLS_POSITION.x;
			_timeBallsContainer.y = TIME_BALLS_POSITION.y;
			_timeBallsContainerContainer.addChild(_timeBallsContainer);
			
			_textContainer = new Sprite;
			_textContainer.x = Constants.WIDTH * TEXT_REL_POSITION_X;
			_textContainer.y = Constants.HEIGHT - TEXT_POSITION_BOTTOM;
			_text = new TextBall;
			_textContainer.addChild(_text);
			addChild(_textContainer);
			
			// Logic
			_timeBalls = new Array;
			addInitialTimeBalls();
			addInitialFixedLiveBalls();
			
			_selectedTimeBall = null;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			function onAddedToStage(ev:Event):void {
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
				stage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
				stage.addEventListener(MouseEvent.MOUSE_MOVE, onStageMouseMove);
			}
		}
		
		private function start():void
		{
			addInitialLiveBalls();
		}
		
		private function addInitialFixedLiveBalls():void
		{
			_fixedInitialLifeBalls = [
				LiveBallLogic.ID_RELACIONES,
				LiveBallLogic.ID_RELACIONES_FAMILIA,
				
				LiveBallLogic.ID_HACER,
				LiveBallLogic.ID_HACER_EXPERIENCIAS,
				
				LiveBallLogic.ID_RELACIONES_AMIGOS,
				
				LiveBallLogic.ID_TENER,
				LiveBallLogic.ID_TENER_COSAS,
				
				LiveBallLogic.ID_HACER_TRABAJO,
				
				LiveBallLogic.ID_SER,
				LiveBallLogic.ID_SER_CUERPO,
			];
		}
		
		private function addInitialLiveBalls():void
		{
			var initialBallID:String = _fixedInitialLifeBalls.shift();
			
			addLiveBall(initialBallID, LiveBallLogic.STATUS_INACTIVE, INITIAL_LIFE_BALL_LIFE);
			
//			addLiveBall(LiveBallLogic.ID_RELACIONES);
//			addLiveBall(LiveBallLogic.ID_HACER);
//			addLiveBall(LiveBallLogic.ID_SER);
//			addLiveBall(LiveBallLogic.ID_TENER);
//			
//			addLiveBall(LiveBallLogic.ID_RELACIONES_AMIGOS);
//			addLiveBall(LiveBallLogic.ID_RELACIONES_FAMILIA);
//			addLiveBall(LiveBallLogic.ID_RELACIONES_PAREJA);
//			
//			addLiveBall(LiveBallLogic.ID_HACER_EXPERIENCIAS);
//			addLiveBall(LiveBallLogic.ID_HACER_SUENO);
//			addLiveBall(LiveBallLogic.ID_HACER_TRABAJO);
//			
//			addLiveBall(LiveBallLogic.ID_SER_ALMA);
//			addLiveBall(LiveBallLogic.ID_SER_CUERPO);
//			addLiveBall(LiveBallLogic.ID_SER_MENTE);
//			
//			addLiveBall(LiveBallLogic.ID_TENER_CASA);
//			addLiveBall(LiveBallLogic.ID_TENER_COSAS);
//			addLiveBall(LiveBallLogic.ID_TENER_DINERO);
//			
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_AMIGOS);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_AMIGOS);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_AMIGOS);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_FAMILIA);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_FAMILIA);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_FAMILIA);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_PAREJA);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_PAREJA);
//			addFinalLiveBall(LiveBallLogic.ID_RELACIONES_PAREJA);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_EXPERIENCIAS);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_EXPERIENCIAS);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_EXPERIENCIAS);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_SUENO);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_SUENO);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_SUENO);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_TRABAJO);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_TRABAJO);
//			addFinalLiveBall(LiveBallLogic.ID_HACER_TRABAJO);
//			addFinalLiveBall(LiveBallLogic.ID_SER_ALMA);
//			addFinalLiveBall(LiveBallLogic.ID_SER_ALMA);
//			addFinalLiveBall(LiveBallLogic.ID_SER_ALMA);
//			addFinalLiveBall(LiveBallLogic.ID_SER_CUERPO);
//			addFinalLiveBall(LiveBallLogic.ID_SER_CUERPO);
//			addFinalLiveBall(LiveBallLogic.ID_SER_CUERPO);
//			addFinalLiveBall(LiveBallLogic.ID_SER_MENTE);
//			addFinalLiveBall(LiveBallLogic.ID_SER_MENTE);
//			addFinalLiveBall(LiveBallLogic.ID_SER_MENTE);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_CASA);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_CASA);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_CASA);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_COSAS);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_COSAS);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_COSAS);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_DINERO);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_DINERO);
//			addFinalLiveBall(LiveBallLogic.ID_TENER_DINERO);
		}
		
		private function addInitialTimeBalls():void
		{
			_removedEnergyByNotAttending = REMOVED_ENERGY_UNATTENDED_CHILDHOOD;
			_addedTimeBalls = 0;
			
			addTimeBall(TimeBallLogic.GOOD_1_TYPE);
			
			addTimeBall(checkProbablity(0.5) ? TimeBallLogic.GOOD_2_TYPE : TimeBallLogic.GOOD_1_TYPE);
			addTimeBall(checkProbablity(0.5) ? TimeBallLogic.GOOD_2_TYPE : TimeBallLogic.GOOD_1_TYPE);
			
			addTimeBall(TimeBallLogic.GOOD_1_TYPE);
			
			addTimeBall(checkProbablity(0.5) ? TimeBallLogic.GOOD_3_TYPE : TimeBallLogic.GOOD_2_TYPE);
			addTimeBall(checkProbablity(0.5) ? TimeBallLogic.GOOD_3_TYPE : TimeBallLogic.GOOD_2_TYPE);
		}
		
		private function addTimeBallToLifeBall(timeBall:TimeBall, lifeBall:LiveBall):void
		{
			timeBall.removeEventListener(MouseEvent.MOUSE_DOWN, onTimeBallMouseDown);
			
			if(_timeBallsTimeline) {
				_timeBallsTimeline.stop();
				_timeBallsTimeline.kill();
			}
			
			// ADD DEPENDING ON TYPE
			// GOOD OR BAD TYPE --------------------------------------------------------------
			if(TimeBallLogic.isGoodType(timeBall) || TimeBallLogic.isBadType(timeBall)) {
				var isGood:Boolean = TimeBallLogic.isGoodType(timeBall);
				
				var valueToTarget:Number = timeBall.value;
				var valueToChildren:Number = 0;
				var childrenLifeBalls:Array
				var childBall:LiveBall;
				var affectedLifeBalls:Array = [lifeBall];
				
				// -> CASE OF MUTATING A LIFE BALL
				if(isGood && lifeBall.status == LiveBallLogic.STATUS_INACTIVE) {
					_isShowingNewLifeBall = true;
					setTimeout(function():void { hideText(); _isShowingNewLifeBall = false; }, 2000);
					refreshTextWithDiscoveredLifeBall(lifeBall);
					
					var parentLifeBall:LiveBall = _lifeBalls[lifeBall.parentID];
					if(parentLifeBall) {
						affectedLifeBalls.push(parentLifeBall);
						valueToTarget = parentLifeBall.removeEnergyByMutating(0.5) + timeBall.value / 2;
					}
					else {
						valueToTarget = timeBall.value / 2;
					}
					
					lifeBall.addEnergy(valueToTarget, 0.5);
				}
				
				// -> CASE BALL IS DEAD
				else if(isGood && lifeBall.status == LiveBallLogic.STATUS_DEAD) {
					valueToTarget = timeBall.value;
					
					lifeBall.addEnergy(valueToTarget, 0.5);
				}
				
				// -> OTHER CASES
				else {
					// - get values for children and target
					if(lifeBall.group == LiveBallLogic.GROUP_PRIMARY) {
						childrenLifeBalls = _lifeBallsSecondaryAssoc[lifeBall.id];
					}
					else if(lifeBall.group == LiveBallLogic.GROUP_SECONDARY) {
						childrenLifeBalls = _lifeBallsFinalAssoc[lifeBall.id];
					}
					valueToTarget = (childrenLifeBalls && childrenLifeBalls.length > 0) ? (timeBall.value / 2) : timeBall.value;
					valueToChildren = valueToTarget;
	
					// - add value to target
					if(isGood) lifeBall.addEnergy(valueToTarget, 0.5);
					else lifeBall.removeEnergy(valueToTarget, 0.5);
					
					// - add values to children
					if(childrenLifeBalls && childrenLifeBalls.length > 0 && valueToChildren != 0) {
						var valueToChild:Number = valueToChildren / childrenLifeBalls.length;
						for(var i:int = 0; i < childrenLifeBalls.length; i++) {
							childBall = (childrenLifeBalls[i] as LiveBall);
							if(childBall.status != LiveBallLogic.STATUS_ACTIVE) continue;
							
							if(isGood) childBall.addEnergy(valueToChild, 0.8);
							else childBall.removeEnergy(valueToChild, 0.8);
						}
						
						affectedLifeBalls = affectedLifeBalls.concat(childrenLifeBalls);
					}
				}
				
				// ANIMATE TIMEBALL
				timeBall.removeAnimation(onCompleteNormal);
				
//				trace(" - ENERGY [" + timeBall.type + "," + lifeBall.id + "]: " + lifeBall.toString());
//				for each(var af:LiveBall in affectedLifeBalls) {
//					trace("   -> affected: " + af.toString());
//				}
				
				function onCompleteNormal():void {
					removeTimeBall(timeBall);
					
					// SUBSTRACT ENERGY TO UNATTENDED
					if(isGood) {
						checkUnattendedLifeBalls(affectedLifeBalls);
					}
					
					// CHECK IF NEW LIFE BALLS MUST BE ADDED
					if(isGood) checkNewLifeBalls(affectedLifeBalls);
					
					// CHECK IF SOME INACTIVE CHILDREN CANNOT MUTATE ANYMORE
					checkUnmutableInactiveLifeBalls();
					
					// CHECK LOSING CONDITIONS
					checkLosingConditions();
					
					// ADD NEW TIME BALLS & REORDER
					checkNewTimeBalls();
					reOrderTimeBalls();
				}
			}
			
			// SPECIAL TYPE --------------------------------------------------------------
			else if(TimeBallLogic.isSpecialType(timeBall)) {
				// ANIMATE TIMEBALL
				timeBall.removeAnimation(onCompleteSpecial);
				
				function onCompleteSpecial():void {
					removeTimeBall(timeBall);
					
					removeLiveBallAndSiblings(lifeBall);
					
					checkFinishinConditions();
					reOrderTimeBalls();
				}
			}
		}
		
		private function getLifeBallsNumber():int
		{
			var res:int = 0;
			
			for(var l:String in _lifeBalls) {
				res++;
			}
			
			return res;
		}
		
		private function checkLosingConditions():void
		{
			if(_isGameLost) return;
			
			var serBall:LiveBall = _lifeBalls[LiveBallLogic.ID_SER];
			var hacerBall:LiveBall = _lifeBalls[LiveBallLogic.ID_HACER];
			var tenerBall:LiveBall = _lifeBalls[LiveBallLogic.ID_TENER];
			var relacionesBall:LiveBall = _lifeBalls[LiveBallLogic.ID_RELACIONES];
			
			if( (serBall && serBall.status == LiveBallLogic.STATUS_DEAD) ||
				(hacerBall && hacerBall.status == LiveBallLogic.STATUS_DEAD) ||
				(tenerBall && tenerBall.status == LiveBallLogic.STATUS_DEAD) ||
				(relacionesBall && relacionesBall.status == LiveBallLogic.STATUS_DEAD)) {
				_isGameLost = true;
			}
		}
		
		private function checkFinishinConditions():void
		{
			if(_isGameFinished) return;
			
			if(_timeBalls.length <= 0) {
				_isGameFinished = true;
				
				setTimeout(function():void { _text.showText("You are dead."); }, 4000);
				setTimeout(function():void { _text.hideText(); }, 6000);
				setTimeout(function():void { dispatchEvent(new Event(Event.COMPLETE)); }, 7000);
			}
		}
		
		private function checkNewTimeBalls():void
		{
			var type:String;
			
			if(_isGameLost) {
				if(!_isGameLostBallsCreated) {
					_isGameLostBallsCreated = true;
					
					addTimeBall(TimeBallLogic.SPECIAL_TENER_TYPE);
					addTimeBall(TimeBallLogic.SPECIAL_HACER_TYPE);
					addTimeBall(TimeBallLogic.SPECIAL_RELACIONES_TYPE);
					addTimeBall(TimeBallLogic.SPECIAL_SER_TYPE);
				}
				
				return;
			}
			
			// BALLS LIMIT PER AGE
			const MAX_BALLS_IN_CHILDHOOD:int = 10;
			const MAX_BALLS_IN_TEENAGER:int = 25;
			const MAX_BALLS_IN_ADULT:int = 50;
			
			// PROBABILITY OF GOOD AND BAR PER AGE
			const PROBABILITY_IN_CHILDHOOD:Object = {
				bad: 0.1, good: 0.9,
				bad1: 0.8, bad2: 0.15, bad3: 0.05,
				good1: 0.5, good2: 0.3, good3: 0.2
			};
			const PROBABILITY_IN_TEENAGER:Object = {
				bad: 0.3, good: 0.7,
				bad1: 0.7, bad2: 0.2, bad3: 0.1,
				good1: 0.6, good2: 0.3, good3: 0.1
			};
			const PROBABILITY_IN_ADULT:Object = {
				bad: 0.5, good: 0.5,
				bad1: 0.6, bad2: 0.25, bad3: 0.15,
				good1: 0.7, good2: 0.15, good3: 0.15
			};
			const PROBABILITY_IN_OLD_AGE:Object = {
				bad: 0.6, good: 0.4,
				bad1: 0.5, bad2: 0.25, bad3: 0.25,
				good1: 0.75, good2: 0.15, good3: 0.1
			};
			var probability:Object;
				
			var lifeBallsNumber:int = getLifeBallsNumber();
			
			// CHILDHOOD
			if(_fixedInitialLifeBalls.length > 0) {
				_removedEnergyByNotAttending = REMOVED_ENERGY_UNATTENDED_CHILDHOOD;
				probability = PROBABILITY_IN_CHILDHOOD;
			}
			// TEENAGER
			else if(lifeBallsNumber < MAX_BALLS_IN_TEENAGER) {
				_removedEnergyByNotAttending = REMOVED_ENERGY_UNATTENDED_TEENAGER;
				probability = PROBABILITY_IN_TEENAGER;
			}
			// ADULT
			else if(lifeBallsNumber < MAX_BALLS_IN_ADULT) {
				_removedEnergyByNotAttending = REMOVED_ENERGY_UNATTENDED_ADULT;
				probability = PROBABILITY_IN_ADULT;
			}
			// OLD AGE
			else {
				_removedEnergyByNotAttending = REMOVED_ENERGY_UNATTENDED_OLD_AGE;
				probability = PROBABILITY_IN_OLD_AGE;
			}
			
			// Get type with probability
			var rnd:Number = Math.random();
			if(checkProbablity(probability.bad)) { // Bad type
				if(rnd <= probability.bad3) {
					type = TimeBallLogic.BAD_3_TYPE;
				}
				else if(rnd <= probability.bad2) {
					type = TimeBallLogic.BAD_2_TYPE;
				}
				else {
					type = TimeBallLogic.BAD_1_TYPE;
				}
			}
			else { // Good type
				if(rnd <= probability.good3) {
					type = TimeBallLogic.GOOD_3_TYPE;
				}
				else if(rnd <= probability.good2) {
					type = TimeBallLogic.GOOD_2_TYPE;
				}
				else {
					type = TimeBallLogic.GOOD_1_TYPE;
				}
			}
			
			addTimeBall(type);
		}
		
		private function checkNewLifeBalls(affectedBalls:Array):void
		{
			// FIRST, CHECK THE FIXED
			if(_fixedInitialLifeBalls.length > 0) {
				var nextFixedID:String = _fixedInitialLifeBalls[0];
				var nextFixedParentID:String = LiveBallLogic.getParentIDForChildLiveBall(nextFixedID);
				var nextFixedParent:LiveBall = _lifeBalls[nextFixedParentID];
				
				if(canHaveChildrenByID(nextFixedParentID) && !isSomeInactiveBallsPresent()) {
					_fixedInitialLifeBalls.shift();
					addLiveBall(nextFixedID, LiveBallLogic.STATUS_INACTIVE, INITIAL_LIFE_BALL_LIFE);
				}
				else {
					var inactiveBalls:Array = getAllLifeBallsByStatus(LiveBallLogic.STATUS_INACTIVE);
					if(inactiveBalls.length > 0) {
						for each(var inactiveBall:LiveBall in inactiveBalls) {
							inactiveBall.needAttentionAnimation();
						}
					}
					else if(nextFixedParent && affectedBalls.indexOf(nextFixedParent) < 0) {
						nextFixedParent.needAttentionAnimation();
					}
				}
				
				function isSomeInactiveBallsPresent():Boolean
				{
					for each(var lifeBall:LiveBall in _lifeBalls) {
						if(lifeBall.status == LiveBallLogic.STATUS_INACTIVE) {
							return true;
						}
					}
					
					return false;
				}
			}
			
			// IF NOT, GO FOR RANDOM
			else {
				var newLifeBallID:String;
				for each(var lifeBall:LiveBall in affectedBalls) {
					if(canHaveChildren(lifeBall)) {
						newLifeBallID = LiveBallLogic.getRandomChildLiveBallID(lifeBall.id);
						if(!newLifeBallID) continue;
						
						addLiveBall(newLifeBallID, LiveBallLogic.STATUS_INACTIVE, INITIAL_LIFE_BALL_LIFE);
						break;
					}
				}				
			}
		}
		
		private function checkUnattendedLifeBalls(affectedBalls:Array):void
		{
			var unattendedBall:LiveBall;
			for(var id:String in _lifeBalls) {
				unattendedBall = _lifeBalls[id];
				
				if(affectedBalls.indexOf(unattendedBall) >= 0) continue;
				if(unattendedBall.status != LiveBallLogic.STATUS_ACTIVE) continue;
				if(haveChildren(unattendedBall)) continue;
				
				unattendedBall.removeEnergyByNotAttended(_removedEnergyByNotAttending);
			}
		}
		
		private function checkUnmutableInactiveLifeBalls():void
		{
			if(_fixedInitialLifeBalls.length <= 0) {
				var inactiveChildren:Array = getAllLifeBallsByStatus(LiveBallLogic.STATUS_INACTIVE);
				
				var parentLifeBall:LiveBall;
				for each(var inactiveChild:LiveBall in inactiveChildren) {
					parentLifeBall = _lifeBalls[inactiveChild.parentID];
					
					if(!parentLifeBall) continue;
					
					if(parentLifeBall.life < MUTATE_LIFE_BALL_LIFE) {
						removeLiveBall(inactiveChild);
					}
				}
			}
		}
		
		// -------------------------------------------------------------------------------
		// TIME BALLS RELATED
		
		private function reOrderTimeBalls():void
		{
			if(_timeBallsTimeline) {
				_timeBallsTimeline.stop();
				_timeBallsTimeline.kill();
			}
			
			const ANIM_PER_BALL_TIME:Number = 0.5;
			const DELAY_ACUM_STEP:Number = 0.2;
			
			_timeBallsTimeline = new TimelineLite({onComplete: onComplete});
			var timeBall:TimeBall;
			var separationAcum:Number = 0;
			var delayAcum:Number = 0;
			for(var i:int = 0; i < _timeBalls.length; i++) {
				timeBall = _timeBalls[i];
				
				_timeBallsTimeline.add(TweenLite.to(
					timeBall, ANIM_PER_BALL_TIME, {x: 0 + separationAcum, y: 0, ease: SineInOut.ease}), delayAcum);
				separationAcum -= TIME_BALLS_SEPARATION;
				delayAcum += DELAY_ACUM_STEP;
			}
			
			function onComplete():void {
				
			}
		}
		
		private function addTimeBall(type:String):void 
		{
			// Create
			var timeBall:TimeBall = TimeBallLogic.createTimeBall(type);
			timeBall.addEventListener(MouseEvent.MOUSE_DOWN, onTimeBallMouseDown);
			
			// Position
			var timeBallPos:Point = new Point;
			var lastTimeBall:TimeBall = _timeBalls.length > 0 ? _timeBalls[_timeBalls.length - 1] : null;
			if(lastTimeBall) {
				timeBallPos.x = lastTimeBall.x - TIME_BALLS_SEPARATION;
				timeBallPos.y = 0;
			}
			timeBall.x = timeBallPos.x; timeBall.y = timeBallPos.y;
			
			// Add
			_timeBalls.push(timeBall);
			_timeBallsContainer.addChild(timeBall);
			
			_addedTimeBalls++;
			if(TimeBallLogic.isGoodType(timeBall)) {
				_lastConsecutiveAddedBadTimeBalls = 0;
				_lastConsecutiveAddedGoodTimeBalls++;
			}
			else if(TimeBallLogic.isBadType(timeBall)) {
				_lastConsecutiveAddedBadTimeBalls++;
				_lastConsecutiveAddedGoodTimeBalls = 0;
			}
			
			trace("Game.ADDTimeBall(): " + timeBall.toString());
			trace(" -> total: " + _addedTimeBalls + ", lastGOOD: " + 
				_lastConsecutiveAddedGoodTimeBalls + ", lastBAD: " + _lastConsecutiveAddedBadTimeBalls);
		}
		
		private function removeTimeBall(timeBall:TimeBall):void
		{
			for(var i:int = 0; i < _timeBalls.length; i++) {
				if(_timeBalls[i] == timeBall) {
					_timeBalls.splice(i, 1);
					break;
				}
			}
			
//			timeBall.removeEventListener(MouseEvent.MOUSE_DOWN, onTimeBallMouseDown);
			_timeBallsContainer.removeChild(timeBall);
		}
		
		private function moveTimeBall(timeBall:TimeBall, stagePosX:Number, stagePosY:Number):void
		{
			_auxPoint.x = stagePosX; _auxPoint.y = stagePosY;
			_auxPoint = _timeBallsContainer.globalToLocal(_auxPoint);
			
			timeBall.x = _auxPoint.x;
			timeBall.y = _auxPoint.y;
		}
		
		private function returnTimeBallToList(timeBall:TimeBall):void
		{
			if(_timeBallsTimeline) {
				_timeBallsTimeline.stop();
				_timeBallsTimeline.kill();
			}
			
			var pos:Point = new Point;
			pos.x = pos.y = 0;
			
			_timeBallsTimeline = new TimelineLite({onComplete: onComplete});
			_timeBallsTimeline.add(TweenLite.to(timeBall, 0.5, {x: pos.x, y: pos.y, ease: BackOut.ease}));
			
			function onComplete():void
			{
				
			}
		}
		
		
		// -------------------------------------------------------------------------------
		// LIVE BALLS RELATED
		
		private function addLiveBall(id:String, status:String, energy:Number = 0):void 
		{
			var ball:LiveBall = LiveBallLogic.createSpecificLiveBall(id, status, energy);
			if(ball.group == LiveBallLogic.GROUP_SECONDARY) {
				if(!_lifeBallsSecondaryAssoc[ball.parentID]) _lifeBallsSecondaryAssoc[ball.parentID] = [];
				
				_lifeBallsSecondaryAssoc[ball.parentID].push(ball);
			}
			else if(ball.group == LiveBallLogic.GROUP_FINAL) {
				if(!_lifeBallsFinalAssoc[ball.parentID]) _lifeBallsFinalAssoc[ball.parentID] = [];
				
				_lifeBallsFinalAssoc[ball.parentID].push(ball);
			}
			
			var posObj:Object = getLiveBallAvailablePosition(ball);
			ball.initialPosition = posObj;
			
			ball.x = posObj.x; ball.y = posObj.y;
			_lifeBallsContainer.addChild(ball);
			ball.showAnimation();
			_lifeBalls[ball.id] = ball;
			
			addLiveBallChildrenPositions(ball);
			
//			trace("Game.ADDLiveBall(): " + ball.toString());
		}
		
		private function addFinalLiveBall(parentID:String, energy:Number = 0):void
		{
			var randomFinalLiveBallID:String = LiveBallLogic.getRandomChildLiveBallID(parentID);
			
			if(!randomFinalLiveBallID) {
				throw new Error("NO MORE AVAILABLE FINAL BALLS!!");
			}
			
			addLiveBall(randomFinalLiveBallID, LiveBallLogic.STATUS_INACTIVE, INITIAL_LIFE_BALL_FINAL_LIFE);
		}
		
		private function getAllLifeBallsByStatus(status:String):Array
		{
			var res:Array = [];
			
			for each(var lifeBall:LiveBall in _lifeBalls) {
				if(lifeBall.status == status) {
					res.push(lifeBall);
				}
			}
			
			return res;
		}
		
		private function getAllLifeBallsOfParent(parentLifeBall:LiveBall):Array
		{
			var res:Array = [parentLifeBall];
			var aux:LiveBall;
			var auxArr:Array;
			
			if(parentLifeBall.group == LiveBallLogic.GROUP_PRIMARY) {
				var assocSec:Array = _lifeBallsSecondaryAssoc[parentLifeBall.id];
				if(assocSec) {
					res = res.concat(assocSec);
					
					for each(aux in assocSec) {
						auxArr = _lifeBallsFinalAssoc[aux.id];
						if(auxArr) res = res.concat(auxArr);
					}
				}
			}
			else {
				throw new Error("TODO");
			}
			
			return res;
		}
		
		private function getLiveBallAvailablePosition(liveBall:LiveBall):Object
		{
			var res:Object;
			
			switch(liveBall.group) {
				case LiveBallLogic.GROUP_PRIMARY:
					res = getRandomItem(_lifeBallsPrimaryPosAvailable);
					break;
				case LiveBallLogic.GROUP_SECONDARY:
					res = getRandomItem(_lifeBallsSecondaryPosAvailable[liveBall.parentID]);
					break;
				case LiveBallLogic.GROUP_FINAL:
					var assocLifeBallsInParent:int = _lifeBallsFinalAssoc[liveBall.parentID].length;
					var initialPosObj:Object = _lifeBallsFinalPosAvailable[liveBall.parentID];
					var initialPosObjXSign:int = (initialPosObj.x / Math.abs(initialPosObj.x));
					
					var parentBall:LiveBall = _lifeBalls[liveBall.parentID];
					var initSeparationX:Number = initialPosObjXSign * (Math.abs(initialPosObj.x) - Math.abs(parentBall.x) / 2);
					
					var posY:Number = initialPosObj.y;
					// ULTRA ÑAPASO!
					if(Math.abs(posY) > 60 && Math.abs(posY) < 180) {
						posY = (-1 * (Math.abs(posY) / posY) * 35) + posY;
					}
					
					res = {
						x: (initSeparationX + initialPosObjXSign * assocLifeBallsInParent * LB_FINAL_SECUENTIAL_OFFSET),
						y: posY
					};
					break;
			}
			
			return res;
		}
		
		private function addLiveBallChildrenPositions(liveBall:LiveBall):void
		{
			var auxPosObj:Object;
			
			switch(liveBall.group) {
				// For primary: add positions of their possible secondaries
				case LiveBallLogic.GROUP_PRIMARY:
					_lifeBallsSecondaryPosAvailable[liveBall.id] = [];
					
					var initialPos:Object = liveBall.initialPosition;
					
					var offset:Object;
					for(var i:int = 0; i < LB_SECONDARY_OFFSETS.length; i++) {
						offset = LB_SECONDARY_OFFSETS[i];
						
						auxPosObj = {
							x: liveBall.x + offset.x * initialPos.xFactor, 
							y: liveBall.y + offset.y * initialPos.yFactor
						};
						_lifeBallsSecondaryPosAvailable[liveBall.id].push(auxPosObj);
					}
					break;
				case LiveBallLogic.GROUP_SECONDARY:
					// - balls that are to the right
					if(liveBall.x > 0) {
						_lifeBallsFinalPosAvailable[liveBall.id] = 
							{x: liveBall.x + LB_FINAL_INITIAL_OFFSET, y: liveBall.y};
					}
					// - balls that are to the left
					else {
						_lifeBallsFinalPosAvailable[liveBall.id] = 
							{x: liveBall.x - LB_FINAL_INITIAL_OFFSET, y: liveBall.y};
					}
					break;
			}
		}
		
		private function canHaveChildrenByID(lifeBallID:String):Boolean
		{
			var res:Boolean = false;
			
			if(lifeBallID == LiveBallLogic.ID_YOU) {
				return true;
			}
			
			var lifeBall:LiveBall = _lifeBalls[lifeBallID];
			if(lifeBallID) {
				res = canHaveChildren(lifeBall);
			}
			
			return res;
		}
		
		private function haveChildren(lifeBall:LiveBall):Boolean
		{
			var res:Boolean = false;
			
			var lifeBallChild:LiveBall;
			var lifeBallChildren:Array;
			
			if(lifeBall.group == LiveBallLogic.GROUP_PRIMARY) {
				lifeBallChildren = _lifeBallsSecondaryAssoc[lifeBall.id];
				res = someWithStatusActive(lifeBallChildren);
			}
			else if(lifeBall.group == LiveBallLogic.GROUP_SECONDARY) {
				lifeBallChildren = _lifeBallsFinalAssoc[lifeBall.id];
				res = someWithStatusActive(lifeBallChildren);
			}
			
			return res;
			
			function someWithStatusActive(chldr:Array):Boolean
			{
				var r:Boolean = false;
				
				if(chldr) {
					for(var i:int = 0; i < chldr.length; i++) {
						if((chldr[i] as LiveBall).status == LiveBallLogic.STATUS_ACTIVE) {
							r = true;
							break;
						}
					}
				}
				
				return r;
			}
		}
		
		private function canHaveChildren(lifeBall:LiveBall):Boolean
		{
			var res:Boolean = false;
			
			var hasChildren:Boolean;
			var hasInactiveChildren:Boolean;
			var lifeBallChildren:Array;
			
			if(lifeBall.life > MUTATE_LIFE_BALL_LIFE) {
				
				if(lifeBall.group == LiveBallLogic.GROUP_PRIMARY) {
					lifeBallChildren = _lifeBallsSecondaryAssoc[lifeBall.id];
					if(!lifeBallChildren) {
						res = true;
					}
					else if(lifeBallChildren.length < MAX_SECONDARY_LIFE_BALLS_PER_PARENT) {
						res = !someWithStatusInactive(lifeBallChildren);
					}
				}
				else if(lifeBall.group == LiveBallLogic.GROUP_SECONDARY) {
					lifeBallChildren = _lifeBallsFinalAssoc[lifeBall.id];
					if(!lifeBallChildren) {
						res = true;
					}
					else if(lifeBallChildren.length < MAX_FINAL_LIFE_BALLS_PER_PARENT) {
						res = !someWithStatusInactive(lifeBallChildren);
					}
				}
			}
			
			return res;
			
			function someWithStatusInactive(chldr:Array):Boolean
			{
				var r:Boolean = false;
				
				for(var i:int = 0; i < chldr.length; i++) {
					if((chldr[i] as LiveBall).status == LiveBallLogic.STATUS_INACTIVE) {
						r = true;
						break;
					}
				}
				
				return r;
			}
		}
		
		private function canAddTimeBallOnLifeBall(timeBall:TimeBall, lifeBall:LiveBall):Boolean
		{
			var res:Boolean = true;
			
			if(TimeBallLogic.isSpecialType(timeBall)) {
				res = timeBall.targetGroups[0] == lifeBall.id;
			}
			else {
				res = res && timeBall.targetGroups.indexOf(lifeBall.group) >= 0;
				
				res = res && (TimeBallLogic.isBadType(timeBall) || lifeBall.life < MAX_LIFE_BALL_LIFE);
				
				res = res && !(TimeBallLogic.isBadType(timeBall) && 
					(lifeBall.status == LiveBallLogic.STATUS_DEAD || lifeBall.status == LiveBallLogic.STATUS_INACTIVE));
			}
			
			return res;
		}
		
		private function canAddSecondaryLifeBall(parentID:String):Boolean
		{
			return _lifeBallsSecondaryAssoc[parentID].length < MAX_SECONDARY_LIFE_BALLS_PER_PARENT;
		}
		
		private function canAddFinalLifeBall(parentID:String):Boolean
		{
			return _lifeBallsFinalAssoc[parentID].length < MAX_FINAL_LIFE_BALLS_PER_PARENT;
		}
		
		private function removeLiveBall(liveBall:LiveBall):void
		{
			delete _lifeBalls[liveBall.id];
			liveBall.hideAnimation(onComplete);
			
			var index:int;
			if(liveBall.group == LiveBallLogic.GROUP_PRIMARY) {
				// SOMETHING?
			}
			
			else if(liveBall.group == LiveBallLogic.GROUP_SECONDARY) {
				index = _lifeBallsSecondaryAssoc[liveBall.parentID].indexOf(liveBall.id);
				
				_lifeBallsSecondaryPosAvailable[liveBall.parentID].push(liveBall.initialPosition);
				
				_lifeBallsSecondaryAssoc[liveBall.parentID].splice(index, 1);
				
				LiveBallLogic.recoverAvailableLiveBall(liveBall.parentID, liveBall.id);
			}
			else if(liveBall.group == LiveBallLogic.GROUP_FINAL) {
				index = _lifeBallsFinalAssoc[liveBall.parentID].indexOf(liveBall.id);
				_lifeBallsFinalAssoc[liveBall.parentID].splice(index, 1);
				
				LiveBallLogic.recoverAvailableLiveBall(liveBall.parentID, liveBall.id);
			}
			
			function onComplete():void {
				_lifeBallsContainer.removeChild(liveBall);
			}
		}
		
		private function removeLiveBallAndSiblings(lifeBall:LiveBall):void
		{
			var allLifeBalls:Array = getAllLifeBallsOfParent(lifeBall);
			var ball:LiveBall;
			
			var tl:TimelineLite = new TimelineLite;
			
			var delay:Number = 0;
			for(var i:int = allLifeBalls.length - 1; i >= 0; i--) {
				ball = allLifeBalls[i];
				tl.call(removeLiveBall, [ball], delay);
				delay += 0.2;
			}
		}
		
		// -------------------------------------------------------------------------------
		// TEXT RELATED
		private function refreshTextWithOverlappedLifeBall(lifeBall:LiveBall = null):void
		{
			if(lifeBall) {
				var txt:String = _selectedTimeBall.text + " on " + getTextOfLifeBallByStatus(lifeBall, false).toLowerCase();
				_text.setText(txt);
			}
			else {
				_text.setText(_selectedTimeBall.text);
			}
		}
		
		private function refreshTextWithDiscoveredLifeBall(lifeBall:LiveBall):void
		{
			if(lifeBall.group == LiveBallLogic.GROUP_PRIMARY) {
				_text.setText("You have " + lifeBall.text.toLowerCase());
			}
			else if(lifeBall.group == LiveBallLogic.GROUP_SECONDARY) {
				_text.setText("You care about " + lifeBall.text.toLowerCase());
			}
			else if(lifeBall.group == LiveBallLogic.GROUP_FINAL) {
				_text.setText("You got " + lifeBall.text.toLowerCase());
			}
		}
		
		private function setTextWithTimeBall(timeBall:TimeBall):void
		{
			_text.showText(timeBall.text);
		}
		
		private function hideText():void
		{
			_text.hideText();
		}
		
		private function setTextWithLifeBall(lifeBall:LiveBall):void
		{
			_text.showText(getTextOfLifeBallByStatus(lifeBall));
		}
		
		private function getTextOfLifeBallByStatus(lifeBall:LiveBall, includeDead:Boolean = true):String
		{
			var res:String;
			
			if(lifeBall.status == LiveBallLogic.STATUS_INACTIVE) {
				res = "???";
			}
			else if(includeDead && lifeBall.status == LiveBallLogic.STATUS_DEAD) {
				res = lifeBall.text + " is in trouble";
			}
			else {
				res = lifeBall.text;
			}
			
			return res;
		}
		
		
		// -------------------------------------------------------------------------------
		// EVENT LISTENERS
		private function onTimeBallMouseDown(ev:MouseEvent):void 
		{
			var timeBall:TimeBall = ev.currentTarget as TimeBall;
			if(timeBall != _timeBalls[0]) return; // Not the first one
			
			_selectedTimeBall = timeBall;
			_selectedTimeBall.dragAnimation();
			setTextWithTimeBall(timeBall);
			
			// Special timeball
			if(TimeBallLogic.isSpecialType(timeBall)) {
				var targetID:String = timeBall.targetGroups[0];
				var targetBall:LiveBall = _lifeBalls[targetID];
				
				targetBall.needAttentionAnimation();
			}
		}
		
		private function onStageMouseUp(ev:MouseEvent):void
		{
			if(_selectedTimeBall) {
				_auxPoint.x = ev.stageX; _auxPoint.y = ev.stageY;
				
				// Check if we drop the Selected Time Ball in any Life Ball
				var selectedLifeBall:LiveBall = getFirstOverlappingObjectOfType(LiveBall, _lifeBallsContainer, _auxPoint) as LiveBall;
				
				// Dropped
				if(selectedLifeBall && canAddTimeBallOnLifeBall(_selectedTimeBall, selectedLifeBall)) {
					addTimeBallToLifeBall(_selectedTimeBall, selectedLifeBall);
				}
				// Not Dropped
				else {
					returnTimeBallToList(_selectedTimeBall);
				}
				
				if(!_isShowingNewLifeBall) hideText();
				
				_selectedTimeBall.dropAnimation();
				_selectedTimeBall = null;
			}
		}
		
		private function onStageMouseMove(ev:MouseEvent):void
		{
			var overlapLifeBall:LiveBall;
			
			// GAME RELATED
			if(_selectedTimeBall) {
				moveTimeBall(_selectedTimeBall, ev.stageX, ev.stageY);
			}
			
			// TEXT RELATED
			if(_selectedTimeBall) {
				if(!TimeBallLogic.isSpecialType(_selectedTimeBall)) {
					_auxPoint.x = ev.stageX; _auxPoint.y = ev.stageY;
					overlapLifeBall = getFirstOverlappingObjectOfType(LiveBall, _lifeBallsContainer, _auxPoint) as LiveBall;
					if(overlapLifeBall) {
						refreshTextWithOverlappedLifeBall(overlapLifeBall);
					}
					else {
						refreshTextWithOverlappedLifeBall();
					}
				}
			}
			
			else if(!_isShowingNewLifeBall){
				_auxPoint.x = ev.stageX; _auxPoint.y = ev.stageY;
				overlapLifeBall = getFirstOverlappingObjectOfType(LiveBall, _lifeBallsContainer, _auxPoint) as LiveBall;
				if(overlapLifeBall) {
					setTextWithLifeBall(overlapLifeBall);
				}
				else {
					hideText();
				}
			}
		}
		
		// -------------------------------------------------------------------------------
		// AUXILS
		private function getFirstOverlappingObjectOfType(clazz:Class, container:DisplayObjectContainer, stagePoint:Point):DisplayObject
		{
			var res:DisplayObject;
			
			var overlappedChild:DisplayObject;
			var overlappedChildren:Array = container.getObjectsUnderPoint(stagePoint);
			
			for(var i:int = 0; i < overlappedChildren.length; i++) {
				overlappedChild = overlappedChildren[i];
				
				while(!(overlappedChild is clazz) && overlappedChild.parent) {
					overlappedChild = overlappedChild.parent;
				}
				
				if(overlappedChild is clazz) {
					res = overlappedChild;
					break;
				}
			}
			
			return res;
		}
	}
}