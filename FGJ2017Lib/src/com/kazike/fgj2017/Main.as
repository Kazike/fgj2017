package com.kazike.fgj2017
{
	import com.kazike.fgj2017.logic.LiveBallLogic;
	import com.kazike.fgj2017.logic.TimeBallLogic;
	import com.kazike.fgj2017.screens.GameScreen;
	
	import flash.display.Stage;

	public class Main
	{
		public function Main(stage:Stage)
		{
			LiveBallLogic.initialize();
			TimeBallLogic.initialize();
			
			var game:GameScreen = new GameScreen;
			stage.addChild(game);
		}
	}
}