package com.kazike.fgj2017.screens
{
	import com.kazike.fgj2017.Button;
	import com.kazike.fgj2017.items.Circle;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class MainScreen extends Sprite
	{
		private var circle:Circle;
		
		public function MainScreen()
		{
			super();
			
			circle = new Circle;
			circle.x = circle.y = 200;
			
			addChild(circle);
			
			var button:Button = new Button;
			button.addEventListener(MouseEvent.CLICK, onButtonClick);
			button.x = 200;
			button.y = 400;
			
			addChild(button);
		}
		
		private function onButtonClick(ev:MouseEvent):void {
			circle.changeColor();
		}
	}
}