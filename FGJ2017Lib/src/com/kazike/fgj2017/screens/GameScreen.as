package com.kazike.fgj2017.screens
{
	import com.kazike.fgj2017.logic.Constants;
	import com.kazike.fgj2017.logic.Game;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class GameScreen extends Sprite
	{
		private var _game:Game;
		private var _intro:Intro;
		private var _end:End;
		
		public function GameScreen()
		{
			super();
			
			initGame();
		}
		
		private function initGame():void {
			_end = new End;
			_end.x = Constants.WIDTH / 2;
			_end.y = Constants.HEIGHT / 2;
			addChild(_end);
			
			_game = new Game;
			_game.addEventListener(Event.COMPLETE, onGameComplete);
			_game.setHidden();
			addChild(_game);
			
			_intro = new Intro;
			_intro.addEventListener(Event.COMPLETE, onIntroComplete);
			_intro.x = Constants.WIDTH / 2;
			_intro.y = Constants.HEIGHT / 2;
			addChild(_intro);
		}
		
		private function onIntroComplete(ev:Event):void
		{
			_intro.removeEventListener(Event.COMPLETE, onIntroComplete);
			removeChild(_intro);
			
			_game.show();
		}
		
		private function onGameComplete(ev:Event):void
		{
			_end.show();
		}
	}
}