package com.kazike.fgj2017.screens
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.kazike.fgj2017.IntroMC;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	public class Intro extends Sprite
	{
		private var _step:int = 0;
		private var _mc:MovieClip;
		private var _blocked:Boolean = false;
		
		public function Intro()
		{
			super();
			
			_mc = new IntroMC;
			addChild(_mc);
			
			initialize();
		}
		
		private function initialize():void
		{
			_blocked = true;
			_mc.text.alpha = 0;
			_mc.button.alpha = 0;
			_mc.playButton.alpha = 0;
			_mc.playButton.visible = false;
			
			_mc.button.addEventListener(MouseEvent.CLICK, onButtonClick);
			_mc.playButton.addEventListener(MouseEvent.CLICK, onButtonClick);
			
			
			// LAUNCH FIRST STEP
			_step = 1;
			setTimeout(checkNextStep, 1000);
		}
		
		private function onButtonClick(ev:MouseEvent):void
		{
			if(_blocked) return;
			_blocked = true;
			
			buttonClickEffect();
		}
		
		private function stepRoutine():void
		{
			TweenLite.to(_mc.text, 1, {alpha: 1, ease: Linear.easeNone});
			TweenLite.to(_mc.button, 1, {delay: 2, alpha: 1, onComplete: function():void { _blocked = false; }, ease: Linear.easeNone });
			TweenLite.to(_mc.playButton, 1, {delay: 2, alpha: 1, onComplete: function():void { _blocked = false; }, ease: Linear.easeNone });
		}
		
		private function endRoutine():void
		{
			TweenLite.to(_mc.curtain, 2, {alpha: 0, ease: Linear.easeNone});
			TweenLite.to(this, 2, {delay: 4, alpha: 0, ease: Linear.easeNone, onComplete: onComplete});
			
			function onComplete():void
			{
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		private function buttonClickEffect():void
		{
			TweenLite.to(_mc.button, 1, {alpha: 0, ease: Linear.easeNone});
			TweenLite.to(_mc.playButton, 1, {alpha: 0, ease: Linear.easeNone});
			TweenLite.to(_mc.text, 1, {delay: 1, alpha: 0, ease: Linear.easeNone});
			
			_step++;
			setTimeout(checkNextStep, 3000);
		}
		
		private function checkNextStep():void
		{
			switch(_step) {
				case 1:
					_mc.text.text = "There is nothing.";
					break;
				case 2:
					_mc.text.text = "You are not.";
					break;
				case 3:
					_mc.text.text = "You are.";
					break;
				case 4:
					_mc.text.text = "You are born.";
					_mc.button.visible = false;
					_mc.playButton.visible = true;
					break;
				case 5:
					endRoutine();
					return;
			}
			
			stepRoutine();
		}
	}
}