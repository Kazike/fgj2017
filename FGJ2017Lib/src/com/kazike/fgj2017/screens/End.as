package com.kazike.fgj2017.screens
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.kazike.fgj2017.EndMC;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	public class End extends Sprite
	{
		private var _mc:MovieClip;
		
		public function End()
		{
			super();
			
			_mc = new EndMC;
			addChild(_mc);
			
			initialize();
		}
		
		private function initialize():void
		{
			_mc.curtain.alpha = 0;
			_mc.rememberText.alpha = 0;
			_mc.serText.alpha = 0;
			_mc.relacionesText.alpha = 0;
			_mc.tenerText.alpha = 0;
			_mc.hacerText.alpha = 0;
			_mc.mercheText.alpha = 0;
		}
		
		public function show():void {
			
			var tl:TimelineLite = new TimelineLite;
			tl.add(TweenLite.to(_mc.curtain, 1, {alpha: 1, ease:Linear.easeNone}));
			tl.add(TweenLite.to(this, 1, {}));
			tl.add(TweenLite.to(_mc.rememberText, 1, {alpha: 1, ease:Linear.easeNone}));
			tl.add(TweenLite.to(this, 1, {}));
			tl.add(TweenLite.to(_mc.serText, 1, {alpha: 1, ease:Linear.easeNone}));
			tl.add(TweenLite.to(_mc.relacionesText, 1, {alpha: 1, ease:Linear.easeNone}));
			tl.add(TweenLite.to(_mc.tenerText, 1, {alpha: 1, ease:Linear.easeNone}));
			tl.add(TweenLite.to(_mc.hacerText, 1, {alpha: 1, ease:Linear.easeNone}));
			
			tl.add(TweenLite.to(this, 2, {}));
			tl.add(TweenLite.to(_mc.rememberText, 1, {alpha: 0, ease:Linear.easeNone}));
			tl.call(function():void {_mc.rememberText.text = "Thanks for playing"});
			tl.add(TweenLite.to(_mc.rememberText, 1, {alpha: 1, ease:Linear.easeNone}));
			
			tl.add(TweenLite.to(this, 1, {}));
			tl.add(TweenLite.to(_mc.mercheText, 1, {alpha: 1, ease:Linear.easeNone}));
		}
	}
}