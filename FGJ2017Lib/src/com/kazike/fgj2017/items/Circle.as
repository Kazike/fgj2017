package com.kazike.fgj2017.items
{
	import com.kazike.fgj2017.CircleMC;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	
	public class Circle extends Sprite
	{
		private var _circle:MovieClip;
		
		public function Circle()
		{
			super();
			
			_circle = new CircleMC;
			addChild(_circle);
		}
		
		public function changeColor():void {
			var myColorTransform:ColorTransform = new ColorTransform;
			myColorTransform.color = Math.random() * 0xFFFFFF;
			
			_circle.body.transform.colorTransform = myColorTransform;
		}
	}
}