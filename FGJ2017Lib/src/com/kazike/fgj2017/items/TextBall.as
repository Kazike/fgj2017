package com.kazike.fgj2017.items
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.kazike.fgj2017.TextMC;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class TextBall extends Sprite
	{
		private var _mc:MovieClip;
		private var _text:TextField;
		private var _tl:TimelineLite;
		
		public function TextBall()
		{
			super();
			
			_mc = createMC();
			_text = _mc.text;
			_text.alpha = 0;
			_text.text = "";
			addChild(_mc);
		}
		
		public function setText(text:String):void
		{
			_text.text = text;
		}
		
		public function showText(text:String, animated:Boolean = true):void
		{
			_text.text = text;
			
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			if(animated) {
				_tl = new TimelineLite;
				_tl.add(TweenLite.to(_text, 0.5, {alpha: 1}));
			}
			else {
				_text.alpha = 1;
			}
		}
		
		public function hideText(animated:Boolean = true):void {
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			if(animated) {
				_tl = new TimelineLite;
				_tl.add(TweenLite.to(_text, 0.5, {alpha: 0}));
			}
			else {
				_text.alpha = 0;
			}
		}
		
		private function createMC():MovieClip
		{
			var res:MovieClip;
			
			res = new TextMC;
			
			return res;
		}
	}
}