package com.kazike.fgj2017.items
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.easing.BackIn;
	import com.greensock.easing.BackOut;
	import com.greensock.easing.Linear;
	import com.greensock.easing.SineIn;
	import com.greensock.easing.SineInOut;
	import com.greensock.easing.SineOut;
	import com.kazike.fgj2017.LifeBallMC;
	import com.kazike.fgj2017.logic.Game;
	import com.kazike.fgj2017.logic.LiveBallLogic;
	import com.kazike.fgj2017.logic.TimeBallLogic;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	
	public class LiveBall extends Sprite
	{
		private const COLOR_DEAD:uint = 0;
		private const COLOR_INACTIVE:uint = 0xFFFFFF;
		
		private const COLOR_RELACIONES:uint = 0xd61d1d;
		private const COLOR_HACER:uint = 0x4286f4;
		private const COLOR_SER:uint = 0xf6ff4c;
		private const COLOR_TENER:uint = 0x4af441;
		
		private const MAX_SCALE:Number = 1;
		private const MIN_SCALE:Number = 0.3;
		
		public var id:String;
		public var parentID:String;
		public var type:String;
		public var group:String;
		public var text:String;
		public var life:Number;
		public var status:String;
		
		public var initialPosition:Object;
		
		private var _groupContainer:Sprite;
		private var _container:Sprite;
		private var _mc:MovieClip;
		private var _tl:TimelineLite;
		
		public function LiveBall(id:String, parentID:String, type:String, group:String, text:String, life:Number, status:String)
		{
			super();
			
			this.id = id;
			this.parentID = parentID;
			this.type = type;
			this.group = group;
			this.text = text;
			this.life = life;
			this.status = status;
			
			_groupContainer = new Sprite;
			_groupContainer.scaleX = _groupContainer.scaleY = getGroupContainerScale(group);
			addChild(_groupContainer);
			
			_container = new Sprite;
			_groupContainer.addChild(_container);
			
			_mc = createMC();
			_container.addChild(_mc);
			
			setShapeWithOwnProperties();
		}
		
		public function addEnergy(value:Number, delay:Number = 0):void
		{
			if(value < 0) throw new Error("USE REMOVE ENERGY!");
			
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			var prevLife:Number = life;
			var newLife:Number = life + value;
			var diff:Number = Math.abs(newLife - prevLife);
			
			life = Math.min(newLife, Game.MAX_LIFE_BALL_LIFE);
			
			_tl = new TimelineLite({delay: delay});
			
			// ACTIVE EFFECT
			if(status == LiveBallLogic.STATUS_INACTIVE || status == LiveBallLogic.STATUS_DEAD) {
				status = LiveBallLogic.STATUS_ACTIVE;
				
				_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 0, scaleY: 0, ease: BackIn.ease}));
				_tl.add(TweenLite.to(_mc, 0.4, {scaleX: 1.05, scaleY: 1.05, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 0.95, scaleY: 0.95, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: BackOut.ease}));
				
				_tl.add(TweenLite.to(_mc, 0.5, {alpha: 0, ease: Linear.easeNone}), 0);
				_tl.call(setShapeWithOwnProperties, [false], 0.5);
				_tl.add(TweenLite.to(_mc, 0.5, {alpha: 1, ease: Linear.easeNone}), 0.5);
			}
			
			// SMALL EFFECT
			else if(diff < Math.abs(TimeBallLogic.GOOD_1_VALUE)) {
				_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 1.05, scaleY: 1.05, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.3, {scaleX: 0.95, scaleY: 0.95, ease: SineOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineOut.ease}));
			}
			// MEDIUM EFFECT
			else if(diff < Math.abs(TimeBallLogic.GOOD_2_VALUE)) {
				_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 1.1, scaleY: 1.1, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.3, {scaleX: 0.95, scaleY: 0.95, ease: SineOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineOut.ease}));
			}
			// BIG EFFECT
			else {
				_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 1.2, scaleY: 1.2, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.3, {scaleX: 0.95, scaleY: 0.95, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: BackOut.ease}));
			}
			
			var newContainerScale:Number = getContainerScale(life);
			_tl.add(TweenLite.to(_container, 0.4, {scaleX: newContainerScale, scaleY: newContainerScale}), 0);
			
			_mc.attentionIndicator.hideIndicator();
			
			showFullLifeIndicator();
		}
		
		public function removeEnergy(value:Number, delay:Number = 0):void
		{
			if(value > 0) throw new Error("USE ADD ENERGY!");
			
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			var prevLife:Number = life;
			var newLife:Number = life + value;
			var diff:Number = Math.abs(newLife - prevLife);
			
			life = Math.max(newLife, 0);
			
			_tl = new TimelineLite({delay: delay});
			
			// DEAD EFFECT
			if(life == 0) {
				status = LiveBallLogic.STATUS_DEAD;
				setShapeWithOwnProperties(false);
				
				_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 0.85, scaleY: 0.85, ease: SineIn.ease}));
				_tl.add(TweenLite.to(_mc, 0.4, {scaleX: 1, scaleY: 1, ease: SineInOut.ease}));
			}
			
			// SMALL EFFECT
			else if(diff < Math.abs(TimeBallLogic.BAD_1_VALUE)) {
				_tl.add(TweenLite.to(_mc, 0.4, {scaleX: 0.95, scaleY: 0.95, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineOut.ease}));
			}
			// MEDIUM EFFECT
			else if(diff < Math.abs(TimeBallLogic.BAD_2_VALUE)) {
				_tl.add(TweenLite.to(_mc, 0.4, {scaleX: 0.8, scaleY: 0.8, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineOut.ease}));
			}
			// BIG EFFECT
			else {
				_tl.add(TweenLite.to(_mc, 0.4, {scaleX: 0.6, scaleY: 0.6, ease: BackOut.ease}));
				_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineOut.ease}));
			}
			
			var newContainerScale:Number = getContainerScale(life);
			_tl.add(TweenLite.to(_container, 0.9, {scaleX: newContainerScale, scaleY: newContainerScale}), 0);
			
			showFullLifeIndicator();
		}
		
		public function removeEnergyByMutating(delay:Number = 0):Number
		{
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			life = life / 2;
			
			_tl = new TimelineLite({delay: delay});
			
			var newContainerScale:Number = getContainerScale(life);
			_tl.add(TweenLite.to(_container, 1.0, {scaleX: newContainerScale, scaleY: newContainerScale, ease: BackOut.ease}), 0);
			
			showFullLifeIndicator();
			
			return life;
		}
		
		public function removeEnergyByNotAttended(value:Number, delay:Number = 0):void
		{
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			var prevLife:Number = life;
			var newLife:Number = life + value;
			
			if(newLife <= 0) {
				removeEnergy(value, delay);
				return;
			}
			
			life = Math.max(newLife, 0);
			
			_tl = new TimelineLite({delay: delay});
			
			var newContainerScale:Number = getContainerScale(life);
			_tl.add(TweenLite.to(_container, 0.6, {scaleX: newContainerScale, scaleY: newContainerScale}), 0);
			
//			_mc.loseLifeIndicator.gotoAndPlay(2);
			
			showFullLifeIndicator();
		}
		
		public function showAnimation(completeHandler:Function = null):void
		{
			_tl = new TimelineLite({onComplete: completeHandler});
			_tl.set(_mc, {scaleX: 0, scaleY: 0});
			
			_tl.add(TweenLite.to(_mc, 0.8, {scaleX: 1, scaleY: 1, ease: BackOut.ease}));
		}
		
		public function hideAnimation(completeHandler:Function = null):void
		{
			_tl = new TimelineLite({onComplete: completeHandler});
			
			_tl.add(TweenLite.to(_mc, 0.8, {scaleX: 0, scaleY: 0, ease: BackIn.ease}));
		}
		
		public function needAttentionAnimation(completeHandler:Function = null):void
		{
//			_tl = new TimelineLite({onComplete: completeHandler});
//			
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1.05, scaleY: 1.05, ease: SineInOut.ease}));
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineInOut.ease}));
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1.05, scaleY: 1.05, ease: SineInOut.ease}));
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineInOut.ease}));
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1.05, scaleY: 1.05, ease: SineInOut.ease}));
//			_tl.add(TweenLite.to(_mc, 0.2, {scaleX: 1, scaleY: 1, ease: SineInOut.ease}));
			
			_mc.attentionIndicator.alpha = 0;
			TweenLite.to(_mc.attentionIndicator, 0.5, {alpha: 1});
			_mc.attentionIndicator.showIndicator();
		}
		
		public function showFullLifeIndicator():void
		{
			if(life >= Game.MAX_LIFE_BALL_LIFE) {
				_mc.stroke.visible = true;
				_mc.stroke.alpha = 0;
				
				TweenLite.to(_mc.stroke, 0.5, {alpha: 1});
			}
			else {
				TweenLite.to(_mc.stroke, 0.5, {alpha: 0});
			}
		}
		
		public override function toString():String
		{
			return id + " - " + type + " - " + group + " - " + status + " - " + life;
		}
		
		private function setShapeWithOwnProperties(modifyScale:Boolean = true):void
		{
			if(modifyScale) {
				_container.scaleX = _container.scaleY = getContainerScale(this.life);
			}
			
			_mc.stroke.visible = false;
			
			switch(status) {
				case LiveBallLogic.STATUS_ACTIVE:
					if(life >= Game.MAX_LIFE_BALL_LIFE) {
						_mc.stroke.visible = true;
						_mc.stroke.alpha = 1
					}
					setBodyColor(getBodyColorWithType());
					break;
				case LiveBallLogic.STATUS_INACTIVE:
					_mc.stroke.visible = true;
					setBodyColor(COLOR_INACTIVE, 0);
					break;
				case LiveBallLogic.STATUS_DEAD:
					setBodyColor(COLOR_DEAD);
					break;
			}
		}
		
		private function getGroupContainerScale(group:String):Number
		{
			var res:Number = 1;
			
			switch(group) {
				case LiveBallLogic.GROUP_PRIMARY:
					res = 1;
					break;
				case LiveBallLogic.GROUP_SECONDARY:
					res = 0.75;
					break;
				case LiveBallLogic.GROUP_FINAL:
					res = 0.5;
					break;
			}
			
			return res;
		}
		
		private function getContainerScale(life:Number):Number
		{
			var res:Number = MIN_SCALE + (life / Game.MAX_LIFE_BALL_LIFE) * (MAX_SCALE - MIN_SCALE);
			return res;
		}
		
		private function createMC():MovieClip
		{
			var res:MovieClip;
			
			res = new LifeBallMC;
			res.stop();
			
			// TODO: BORRAR
			switch(group)
			{
				case LiveBallLogic.GROUP_PRIMARY:
					break;
				case LiveBallLogic.GROUP_SECONDARY:
					res.scaleX = res.scaleY = 0.8;
					break;
				case LiveBallLogic.GROUP_FINAL:
					res.scaleX = res.scaleY = 0.5;
					break;
				
			}
			
			return res;
		}
		
		private function getBodyColorWithType():uint
		{
			var res:uint = 0;
			
			switch(type) {
				case LiveBallLogic.TYPE_RELACIONES:
					res = COLOR_RELACIONES;
					break;
				case LiveBallLogic.TYPE_HACER:
					res = COLOR_HACER;
					break;
				case LiveBallLogic.TYPE_SER:
					res = COLOR_SER;
					break;
				case LiveBallLogic.TYPE_TENER:
					res = COLOR_TENER;
					break;
			}
			
			return res;
		}
		
		private function setBodyColor(color:uint, alpha:Number = 1):void
		{	
			var my_color:ColorTransform = new ColorTransform();
			my_color.color = color;
			my_color.alphaMultiplier = alpha;
			
			_mc.body.transform.colorTransform = my_color;
		}
	}
}