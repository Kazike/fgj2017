package com.kazike.fgj2017.items
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.BackIn;
	import com.greensock.easing.Linear;
	import com.kazike.fgj2017.TimeBallMC;
	import com.kazike.fgj2017.logic.TimeBallLogic;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	public class TimeBall extends Sprite
	{
		public var type:String;
		public var targetGroups:Array = new Array;
		public var text:String;
		public var value:Number;
		
		private var _container:Sprite;
		private var _mc:MovieClip;
		private var _tl:TimelineLite;
		private var _tRot:TweenMax;
		
		public function TimeBall(type:String, targetGroups:Array, text:String, value:Number)
		{
			super();
			
			this.type = type;
			this.targetGroups = targetGroups;
			this.text = text;
			this.value = value;
			
			_container = new Sprite;
			addChild(_container);
			
			var randomTime:Number = 5 + Math.random() * 5;
			_tRot = new TweenMax(_container, randomTime, {rotation: 360, repeat: -1, ease: Linear.easeNone});
			_tRot.progress(Math.random());
			
			_mc = createMC();
			_container.addChild(_mc);
		}
		
		public function dragAnimation(completeHandler:Function = null):void
		{
			TweenLite.to(_mc, 0.5, {alpha: 0.7});
		}
		
		public function dropAnimation(completeHandler:Function = null):void
		{
			TweenLite.to(_mc, 0.5, {alpha: 1});
		}
		
		public function removeAnimation(completeHandler:Function = null):void
		{
			if(_tl) {
				_tl.stop(); _tl.kill();
			}
			
			_tl = new TimelineLite({onComplete: onComplete});
			_tl.add(TweenLite.to(_mc, 0.5, {scaleX: 0, scaleY: 0, ease: BackIn.ease}));
			
			function onComplete():void
			{
				TweenMax.killTweensOf(_container);
				
				if(_tl) {
					_tl.stop(); _tl.kill();
				}
				
				if(completeHandler != null) completeHandler();
			}
		}
		
		public override function toString():String
		{
			return type + " - " + value;
		}
		
		private function createMC():MovieClip
		{
			var res:MovieClip;
			
			res = new TimeBallMC;
			
			res.goodDeco.visible = false;
			res.greatDeco.visible = false;
			
			if(TimeBallLogic.isGoodType(this)) {
				res.gotoAndStop("good");
				
				res.goodDeco.visible = (type == TimeBallLogic.GOOD_2_TYPE || type == TimeBallLogic.GOOD_3_TYPE);
				res.greatDeco.visible = (type == TimeBallLogic.GOOD_3_TYPE);
			}
			else if(TimeBallLogic.isBadType(this)) {
				res.gotoAndStop("bad");
				
				res.goodDeco.visible = (type == TimeBallLogic.BAD_2_TYPE || type == TimeBallLogic.BAD_3_TYPE);
				res.greatDeco.visible = (type == TimeBallLogic.BAD_3_TYPE);
			}
			else if(TimeBallLogic.isSpecialType(this)) {
				res.gotoAndStop("special");
				
				res.goodDeco.visible = true;
				res.greatDeco.visible = true;
			}
			
			return res;
		}
	}
}